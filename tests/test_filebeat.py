import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_filebeat_running_and_enabled(Service):
    filebeat = Service("filebeat")
    assert filebeat.is_running
    assert filebeat.is_enabled
