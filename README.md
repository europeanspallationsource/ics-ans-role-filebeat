ics-ans-role-filebeat
===================

Ansible role to install filebeat.

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```yaml
filebeat_logstash_host: localhost:5044
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-filebeat
```

License
-------

BSD 2-clause
